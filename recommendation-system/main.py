import pandas as pd


def get_similar_score(movie_name, user_rating):
    sdf = pd.read_csv("./similarity_scores.csv", index_col="title")
    print((sdf[movie_name] * (user_rating - 2.5)
           ).sort_values(ascending=False)[:20])

    sdf.to_json("./test.json")


def standardize(row):
    return (row - row.mean())/(row.max() - row.min())


def main():
    movies = pd.read_csv("./ml-latest-small/movies.csv")
    ratings = pd.read_csv("./ml-latest-small/ratings.csv")
    df = pd.merge(movies, ratings)
    df = df[["userId", "title", "rating"]]
    df = df.pivot_table(index=["userId"], columns=["title"], values="rating")
    df = df.dropna(thresh=10, axis=1).fillna(0)
    item_similarity = df.corr(method="pearson")
    item_similarity.to_csv("./similarity_scores.csv")


if __name__ == '__main__':
    # main()
    get_similar_score("Fast & Furious (Fast and the Furious 4, The) (2009)", 5)
