import numpy as np
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Embedding, LSTM, Dropout, Activation, Dense, TimeDistributed
from tensorflow.keras.callbacks import EarlyStopping, ModelCheckpoint


def main():
    BATCH_SIZE = 16
    SEQ_LENGTH = 64
    file = open("./data/jigs.txt")
    text = file.read()
    char_to_idx = {ch: i for (i, ch) in enumerate(sorted(list(set(text))))}
    vocab_size = len(char_to_idx)
    data = np.asarray([char_to_idx[c] for c in text], dtype=np.int32)
    model = Sequential()
    model.add(Embedding(vocab_size, 512,
                        batch_input_shape=(BATCH_SIZE, SEQ_LENGTH)))
    model.add(LSTM(256, return_sequences=True, stateful=True))
    model.add(Dropout(0.2))
    model.add(LSTM(256, return_sequences=True, stateful=True))
    model.add(Dropout(0.2))
    model.add(LSTM(256, return_sequences=True, stateful=True))
    model.add(Dropout(0.2))
    model.add(TimeDistributed(Dense(vocab_size)))
    model.add(Activation("softmax"))
    model.summary()
    model.compile(loss="categorical_crossentropy",
                  optimizer="adam", metrics=["accuracy"])
    callbacks = [EarlyStopping(monitor='val_loss', patience=2), ModelCheckpoint(
        filepath='best_model_mnist.h5', monitor='val_loss', save_best_only=True)]
    """
    # callbacks = callbacks
    # history = model.(xTrain, yTrain, batch_size=128, epochs=20,
    #                     verbose=1, validation_data=(xTest, yTest), callbacks=callbacks)
    """


if __name__ == "__main__":
    main()
