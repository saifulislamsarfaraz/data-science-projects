import numpy as np
from sklearn.datasets import load_iris
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score
from sklearn.linear_model import LogisticRegression
from collections import Counter
np.random.seed(42)


def main():
    iris_df = load_iris()
    x = iris_df["data"]
    x = (x - x.mean(axis=0)) / x.std(axis=0)
    y = iris_df["target"]
    x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=0.2)
    n_weak_classifier = 5
    clf_arr = []
    for _ in range(n_weak_classifier):
        idx_list = np.random.randint(
            low=0, high=x_train.shape[0], size=np.random.randint(low=0, high=x_train.shape[0]))
        data = x_train[idx_list, :]
        target = y_train[idx_list]
        clf = LogisticRegression()
        clf.fit(data, target)
        clf_arr.append(clf)
    y_pred_list = []
    for clf in clf_arr:
        y_pred_list.append(clf.predict(x_test))
    _len = len(y_pred_list[0])

    final_pred = []
    for idx in range(_len):
        temp = []
        for y_pred in y_pred_list:
            temp.append(y_pred[idx])
        ans = Counter(temp).most_common()[0][0]
        final_pred.append(ans)

    print("Final Score: ", accuracy_score(y_test, final_pred))


if __name__ == "__main__":
    main()
